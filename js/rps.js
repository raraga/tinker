// Use a callback to pass in our initial funciton

function getRpsWinner(played) {
    let winner;
    switch(played) {
        case 'rock':
        case 'Rock':
            winner = 'Paper';
            break;
        case 'paper':
        case 'Paper':
            winner = 'Scissors';
            break;
        case 'scissors':
        case 'Scissors':
            winner = 'Rock';
            break;
        default:
            console.log('Are we playing Rock, Paper, Scissors? I don\'t know what you are trying to pick.');
    }
    console.log('You played: ' + played + '. ' + winner + ' would beat you.')
}

function playRps(played, getRpsWinner) {
    getRpsWinner(played);
}

playRps('Paper', getRpsWinner);

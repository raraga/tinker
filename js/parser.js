const j = {
    "data": [
        {"foo": [1,2,3] },
        {"bar": [4,5,6] },
        {"baz": [7,8,9] }
    ]
};

class Parser {
    constructor(arr) {
        this.arr = arr;
    }

    get firstValue()
    {
        return this.arr[0];
    }
}

const p = new Parser(j.data[0].foo);
console.log(p.firstValue);
